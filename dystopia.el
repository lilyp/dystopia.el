;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This game is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or (at
;; your option) any later version.
;;
;; This game is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this game.  If not, see <http://www.gnu.org/licenses/>.

(require 'tsukundere/support/peg)

;; Actual code starts here
(set-current-module
  (define-module* '(dystopia)
    (: exports) '(game-name window-width window-height init!)))

(defun tsun:current-window ()
  (eval-scheme ((@ (tsukundere) current-window)) (current-module)))

(defmacro setf-1 (place val)
  `(funcall (funcall (@ (guile) setter)
                     (symbol-function ',(car place)))
            ,@(cdr place)
            ,val))

(defmacro setf (&rest args)
  (let ((sets nil))
    (while args
      (setq sets (cons `(setf-1 ,(car args) ,(cadr args)) sets))
      (setq args (cddr args)))
    (cons 'progn (nreverse sets))))

(define-peg-pattern %id body (+ (or (peg "[a-z]") (peg "[0-9]"))))
(define-peg-pattern id all %id)
(define-peg-pattern long-id all (and (* (and %id "-")) %id))
(define-peg-pattern image all
  (and (* (and id "-")) id (or ".jpg" ".png")))
(define-peg-pattern image/long-id all
  (and long-id (or ".jpg" ".png")))
(define-peg-pattern sound all
  (and (* (and id "-")) id (or ".flac" ".mp3" ".ogg" ".wav")))
(define-peg-pattern sound/long-id all
  (and long-id (or ".flac" ".mp3" ".ogg" ".wav")))

(define-peg-pattern mood all %id)
(define-peg-pattern mood+ext all
  (and (* (and mood "-")) mood (ignore (or ".jpg" ".png"))))

(defun round (num) ; Elisp round
  (funcall (@ (guile) inexact->exact)
           (fround num)))

(fset 's32vector (@ (guile) s32vector))
(import-module! (guile) guile:)
(import-module! (tsukundere) tsun:)
(import-module! (sdl2 blend-mode) sdl:)
(import-module! (sdl2 image) sdl:)
(import-module! (sdl2 rect) sdl:)
(import-module! (sdl2 render) sdl:)
(import-module! (sdl2 surface) sdl:%)
(import-module! (sdl2 ttf) sdl:)
(import-module! (sdl2 video) sdl:)

(defmacro load-person (var name pat attributes)
  `(prog1
       (set ',var (tsun:load-person
                   (concat
                    "img/"
                    (funcall (@ (guile) symbol->string) ',var))
                   (get ',pat 'peg) ,attributes
                   (funcall (@ (guile) symbol->keyword) 'display-name)
                   ,name))
     (fset ',var (tsun:person->procedure ,var))))

(defmacro make-color (r g b a)
  `(eval-scheme ((@ (sdl2) make-color) ,r ,g ,b ,a) (current-module)))

(defvar window-width 1280)
(defvar window-height 720)
(defvar game-name "dystopia.el")

(defun play-sound (id)
  (tsun:play-sound (cdr (assq id sfx))))

(defun play-music (id &rest args)
  (apply (symbol-function 'tsun:play-music)
         (and id (cdr (assq id music)))
         args))

(defun title ()
  (put 'title 'action nil)
  (tsun:call/paused
   (lambda ()
     (update-window-icon 'surprised)
     (play-music 'title)
     (while (not (get 'title 'action))
       (tsun:pause)))
   (: scene) title
   (: mouse-move)
   (lambda (clicks pos rel)
     (put 'title 'action
          (cond
           ((< (funcall (@ (guile) s32vector-ref) pos 0) 560) 'new-game)
           ((> (funcall (@ (guile) s32vector-ref) pos 0) 720) 'credits)
           (t nil)))
     (setf (texture (get 'title 'entity))
           (cdr (or (assq (get 'title 'action) (get 'title 'textures))
                    (assq 'default (get 'title 'textures)))))))
  (get 'title 'action))

(fset 'texture
      (funcall (@ (tsukundere components) component->procedure)
               (@ (tsukundere components) *registry*)
               'texture))

(fset 'transition:alpha
      (funcall (@ (tsukundere script transitions) transition->procedure)
               'alpha))

(defun reset-fade ()
  (put 'fade 'alpha 0)
  (put 'fade 'horizontal-start 0)
  (put 'fade 'horizontal-end 0)
  (put 'fade 'vertical-start 0)
  (put 'fade 'vertical-end 0))

(defun show-credits ()
  (tsun:call/paused
   (lambda ()
     (update-window-icon 'happy)
     (play-music 'credits)
     (tsun:clear! 'background)
     (mapc (lambda (%credits)
             (transition:alpha 2500 (tsun:entity-at (tsun:current-scene)
                                                    'background)
                               %credits t)
             (tsun:sleep 2500))
           (get credits 'credits)))
   (: key-press) (lambda (&rest _) t)
   (: key-release) (lambda (&rest _) t)
   (: mouse-press) (lambda (&rest _) t)
   (: mouse-release) (lambda (&rest _) t)
   (: scene) credits
   (: hard-pause?) t)
  t)

(defun script-loop ()
  (forever
    (let ((action (title)))
      (cond
       ((eq action 'new-game)
        (reset-history!)
        (script))
       ((eq action 'credits) (show-credits))
       ;; We land here during load
       (t (script))))))

(defun window-title ()
  (let ((window-title (funcall (@ (guile) gethostname))))
    (if window-title (concat "dystopia@" window-title) "dystopia")))

(defvar window-icons nil)
(defun update-window-icon (icon)
  (let ((%icon (assq icon window-icons)))
    (and %icon (sdl:set-window-icon! (tsun:current-window) (cdr %icon)))))

(defun init! ()
  (sdl:set-window-title! (tsun:current-window) (window-title))
  (setq window-icons (tsun:load-assets (symbol-function 'sdl:load-image)
                                       "img/icon" (get 'mood+ext 'peg) '(mood)))
  (update-window-icon 'surprised)

  (setq scene (tsun:make-scene '(background foreground fade text menu)))
  (setq credits (tsun:make-scene '(background)))

  (put credits 'credits
       (mapcar
        (lambda (img+text)
          (cons
           (tsun:texture->entity (tsun:load-image (car img+text))
                                 (: position) (s32vector 0 0)
                                 (: anchor) 'top-left)
           (mapcar (lambda (text)
                     (apply (symbol-function 'tsun:make-text)
                            (append text
                                    `(,(: font) "Monospace 20"
                                      ,(: stroke-width) 0.75
                                      ,(: markup?) ,t))))
                (cdr img+text))))
        `(("img/credits/page1.png"
           (,(s32vector 350 100)
            "<b><big>Story</big></b>\nLiliana Prikler"
            ,(: position) ,(s32vector 200 100))
           (,(s32vector 350 100)
            "<b><big>Code</big></b>\nLiliana Prikler"
           ,(: position) ,(s32vector 200 200)))
          ("img/credits/page2.png"
           (,(s32vector 350 200)
           "\
<b><big>Characters</big></b>
DejiNyucu
MUGかぶり
Wifom
z04 (@azworth)"
           ,(: position) ,(s32vector 850 100))
           (,(s32vector 350 150)
           "\
<b><big>Backgrounds</big></b>
Konnet
Uncle Mugen"
           ,(: position) ,(s32vector 850 300)))
          ("img/credits/page3.png"
           (,(s32vector 350 150)
           "\
<b><big>Sound Effects</big></b>
Oiboo
qubodup
rubberduck"
           ,(: position) ,(s32vector 200 100))
           (,(s32vector 500 300)
            "\
<b><big>Music</big></b>
Agecaf
Alexander Ehlers
Cynicmusic
Harm-Jan \"3xBlast\" Wiechers
Kistol
nene
TERNOX"
           ,(: position) ,(s32vector 200 250))))))

  (setq title (tsun:make-scene '(title)))
  (put 'title 'textures
       (tsun:load-images "img/title" (get 'image/long-id 'peg) '(long-id)))
  (put 'title 'entity
       (tsun:texture->entity (cdr (assq 'default (get 'title 'textures)))
                             (: anchor) 'top-left))
  (tsun:scene-add! title 'title (get 'title 'entity))

  (setq backgrounds
        (tsun:load-images "img/bg" (get 'image 'peg) '(id)))
  (setq sfx (tsun:load-sounds "sfx" (get 'sound/long-id 'peg) '(long-id)))
  (setq music (tsun:load-music* "music" (get 'sound 'peg) '(id)))
  (load-person yuu "Yuu" mood+ext '(mood))
  (load-person misaki "Misaki" mood+ext '(mood))
  (load-person eri "Eri" mood+ext '(mood))
  (load-person nozomi "Nozomi" mood+ext '(mood))
  (load-person sensei "Ms. Sensei" mood+ext '(mood))
  (fset 'yuu
        (lambda (&rest args)
          (let ((result (apply (tsun:person->procedure yuu) args)))
            (while args
              (when (eq (car args) (: mood))
                (update-window-icon (cadr args)))
              (setq args (cdr args)))
            result)))
  (tsun:scene-add! scene 'text
                   (tsun:texture->entity (tsun:load-image "img/textbox.png")
                                         (: position) (s32vector 0 720)
                                         (: anchor) 'bottom-left))
  (tsun:scene-add! scene 'text
                   (tsun:make-text (s32vector 664 30)
                                   (: font) "Monospace Bold 20"
                                   (: name) 'speaker
                                   (: stroke-width) 0
                                   (: position) (s32vector 292 540)))
  (tsun:scene-add! scene 'text
                   (tsun:make-text (s32vector 664 230)
                                   (: font) "Monospace 16"
                                   (: name) 'text
                                   (: stroke-width) 0
                                   (: position) (s32vector 268 570)))
  (setq alpha-fade (tsun:make-entity))
  (setq horizontal-fade (tsun:make-entity))
  (setq vertical-fade (tsun:make-entity))
  (reset-fade)
  (setf (tsun:render-function alpha-fade)
        (lambda (&rest _)
          (sdl:set-renderer-draw-blend-mode!
           (tsun:current-renderer)
           sdl:blend)
          (sdl:set-renderer-draw-color!
           (tsun:current-renderer)
           0 0 0 (get 'fade 'alpha))

          (sdl:fill-rect (tsun:current-renderer) nil)
          (sdl:set-renderer-draw-color!
           (tsun:current-renderer)
           0 0 0 255))
        (tsun:render-function horizontal-fade)
        (lambda (&rest _)
          (let ((start (get 'fade 'horizontal-start))
                (end (get 'fade 'horizontal-end)))
            (sdl:fill-rect (tsun:current-renderer)
                           (sdl:make-rect start 0 (- end start) window-height))))
        (tsun:render-function vertical-fade)
        (lambda (&rest _)
          (let ((start (get 'fade 'vertical-start))
                (end (get 'fade 'vertical-end)))
            (sdl:fill-rect (tsun:current-renderer)
                           (sdl:make-rect 0 start window-width (- end start))))))

  (tsun:scene-add! scene 'fade alpha-fade)
  (tsun:scene-add! scene 'fade horizontal-fade)
  (tsun:scene-add! scene 'fade vertical-fade)

  (tsun:init-menu! (: scene) scene
                   (: position) (s32vector 640 270)
                   (: spacing) (s32vector 0 50)
                   (: button)
                   (tsun:make-button
                    `((none . ,(tsun:load-image "img/button.png"))
                      (selected . ,(tsun:load-image "img/button-selected.png")))
                    `((none . ,(make-color 255 255 255 255))))
                   (: text-font) "Monospace Bold 18"
                   (: text-stroke-width) 0)

  (tsun:init-script!
   (symbol-function 'script-loop)
   scene))

(defun bg (id &optional pos)
  (setq bg (tsun:texture->entity (cdr (assq id backgrounds))
                                 (: position)
                                 (or pos (tsun:current-window-center))))
  (tsun:clear! 'background)
  (tsun:add! 'background bg))

(defun narrate (msg &optional append?)
  (tsun:say "" msg append?))

(defun students (msg &optional append?)
  (tsun:say "Students" msg append?))

(defun affection (person &optional delta)
  (if delta
      (put person 'affection (+ (get person 'affection) delta))
    (get person 'affection)))

(defun script ()
  (tsun:clear! 'background)
  (tsun:clear! 'foreground)
  (put eri 'affection 0)
  (put misaki 'affection 69)
  (put nozomi 'affection 0)
  (put sensei 'affection 0)
  (reset-fade)
  (play-music 'yuu)
  (update-window-icon 'normal)
  (intro)
  (first-day)
  (narrate "Yuu ought to save now.")
  (tsun:pause)
  (tsun:call/paused
   (lambda ()
     (play-music nil)
     (tsun:tween 2000 0 window-height
                 (lambda (end) (put 'fade 'vertical-end (round end))))
     (with-pauses
       (narrate "This is as far as the demo goes.")
       (narrate "What? You thought it'd be longer?")
       (narrate
        "Don't get your hopes up high, Yuu will just be disappointed."))))
  (show-credits))

(defun intro (&rest _)
    (with-pauses
    (narrate "I am Yuu.")
    (narrate "Yuu are me.")
    (progn
      (tsun:add! 'foreground (yuu (: position) (s32vector 640 1120)
                                  (: mood) 'normal))
      (yuu "This is what Yuu look like.")
      (tsun:tween 3000 1120 480
                  (lambda (y)
                    (tsun:move! yuu (s32vector 640 (round y))))))
    (yuu (: mood) 'dere "Aren't Yuu kawaii?")
    (progn
      (bg 'dorm/night (s32vector 1280 360))
      (yuu (: mood) 'normal "This is where Yuu sleep at.")
      (tsun:tween 4000 1920 640
                  (lambda (x) (tsun:move! bg (s32vector (round x) 360)))))
    (yuu (: mood) 'happy "Yuu find it very comfortable there.")
    (yuu (: mood) 'sad "But Yuu are lonely, because Yuu are the only one \
in the dorm who does not have a roommate.")
    (yuu "Also, Yuu don't have any parents.")
    (yuu "What are Yuu, some kind of protagonist?")
    (yuu (: mood) 'happy "At least Yuu are not male and Yuu have a face.")
    (progn
      (play-sound 'knock)
      (yuu (: mood) 'surprised)
      (tsun:say "???" "Stop speaking to yourself and go to bed!"))
    (yuu (: mood) 'mad "Ugh, fine.")
    (progn
      (play-music nil)
      (tsun:tween 1000 0 255
                  (lambda (alpha) (put 'fade 'alpha (round alpha))))
      (yuu "Zzz…"))
    (yuu "…" t)
    (progn
      (bg 'dorm/day)
      (yuu "…" t))
    (progn
      (yuu "…" t)
      (play-sound 'knock)
      (tsun:say "???" "Hey, wake up already!")
      (tsun:tween 1000 255 0
                  (lambda (alpha) (put 'fade 'alpha (round alpha)))))
    (progn
      (play-sound 'knock)
      (tsun:say "???" "Open up, you little shit!"))
    (progn
      (play-sound 'creak)
      (narrate "Reluctantly, Yuu open the door."))
    (progn
      (play-music 'nozomi)
      (tsun:add! 'foreground (nozomi (: position) (s32vector 1280 380)
                                     (: mood) 'normal))
      (narrate
       "In comes Nozomi, the student council president, acting as if \
everything was normal.")
      (tsun:tween 2000 1280 1000
                  (lambda (x)
                    (tsun:move! nozomi (s32vector (round x) 380)))))
    (narrate "Of course, Yuu are still mad at her for waking Yuu up so early \
(and also for calling Yuu a little shit, probably), but there's nothing Yuu \
can do against her power.")
    (nozomi (: mood) 'pouting "Why is it that when someone stays up late or \
doesn't wake up, it's always Yuu?")
    (nozomi
     "I am really doing my best to ensure that all students follow the rules…")
    (yuu (: mood) 'angry "It's because you wake us up at 6am when school \
starts at 9am and the school building is <b>right there in the fucking \
background</b>!" (: markup?) t)
    (yuu "Some people need more sleep than that.")
    (nozomi (: mood) 'blushing "You make that sound as if I did something bad.")
    (yuu "Because you do!")
    (nozomi (: mood) 'normal
            "I think you'll find my demands perfectly reasonable.")
    (nozomi
     "Go to bed at 10pm, then you won't have any problems waking up at 6am.")
    (nozomi "Do your homework early enough and you won't have problems \
finishing it before 10pm.")
    (nozomi "Pay attention in school, so that you won't have any troubles \
doing your homework.")
    (nozomi
     "Wake up at 6am, so that you are awake and pay attention in school.")
    (nozomi
     "It's a perfect cycle and all is well, as long as you can stay inside.")
    (yuu "Yeah, and once you fall ever so slightly outside you're on a \
spiral ending in certain doom.")
    (nozomi
     "Which is why the student council has to ensure that doesn't happen.")
    (yuu (: mood) 'surprised
         "I thought the job of the student council was to listen to the \
demands of students and seriously address them.")
    (nozomi (: mood) 'blushing "Pfft, silly Yuu.")
    (progn
      (yuu (: mood) 'mad)
      (nozomi (: mood) 'normal "The job of the student council is to make it \
so that students have no concerns to voice."))
    (yuu (: mood) 'surprised "So you're just silencing everyone?")
    (nozomi (: mood) 'blushing "Silencing is such a strong word.")
    (progn
      (yuu (: mood) 'angry)
      (nozomi
       (: mood) 'normal
       "We only provide proper guidance for those, who steer away from the \
correct path."))
    (nozomi
     "Sometimes, all it takes for a student to see the error of their ways, \
is a few helpful words.")
    (nozomi "Other times we need to take more… thorough methods.")
    (nozomi
     "If students are not properly disciplined at a young age, they will never \
grow up to be the corporate slaves our economy requires.")
    (nozomi
     "And if our economy doesn't have enough corporate slaves, or worse if \
they revolt, then our society will crumble.")
    (nozomi (: mood) 'blushing "You don't want society to crumble, do you?")
    (if (menu revolution?
              `(("Maybe not." ,nil)
                ("Actually…" ,t)))
        (progn
          (affection nozomi -1)
          (narrate "Now, that she phrases it like that, Yuu kinda do.")
          (tsun:pause)
          (narrate "But Yuu don't have enough comrades for that."))
      (progn
        (affection nozomi +1)
        (narrate "Yuu suddenly wonder, what Nozomi's boots taste like.")))
    (nozomi (: mood) 'normal "Not that it matters much.")
    (nozomi "I'll make sure you attend school properly.")
    (progn
      (play-music nil)
      (tsun:tween 2000 0 window-width
                  (lambda (end) (put 'fade 'horizontal-end (round end))))
      (narrate "Thus Yuu are forced to attend school.")
      (tsun:clear! 'foreground)
      (tsun:add! 'foreground (eri (: position) (s32vector 320 420)
                                  (: mood) 'normal))
      (tsun:add! 'foreground (yuu (: position) (s32vector 960 480)))
      (bg 'classroom/day))
    (progn
      (narrate "Approximately an hour later…")
      (play-music 'eri)
      (tsun:tween 2000 0 window-width
                  (lambda (start) (put 'fade 'horizontal-start (round start))))
      (put 'fade 'horizontal-start 0)
      (put 'fade 'horizontal-end 0))))

(defun first-day (&rest _)
  (with-pauses
    (if (history revolution?)
        (narrate "Yuu are still mad.")
      (narrate "Yuu still wonder, what Nozomi's boots taste like."))
    (tsun:say "???" "Oh, hey, Yuu're the transfer student, right?")
    (yuu (: mood) 'normal "That's right, I am Yuu.")
    (yuu "Nice to meet you, uhm…")
    (eri (: mood) 'shy "…")
    (yuu (: mood) 'sad
         "Look, I don't want to assume your name, but the script says…")
    (eri (: mood) 'blushing "…")
    (yuu (: mood) 'happy "I can call you Eri, right?")
    (eri (: mood) 'shy "If you're fine talking to me, I guess…")
    (yuu (: mood) 'normal "Why would I not be?")
    (eri (: mood) 'blushing "…")
    (yuu (: mood) 'sad "I see, you're not a woman of many words.")
    (progn
      (narrate "Strike a conversation regardless?")
      (menu maybe-eri-is-interested-in
            `(("Talk about the weather" smalltalk)
              ,@(and (history revolution?)
                     '(("Complain about school" skipping-classes)
                       ("Complain about Nozomi"
                        challenging-the-student-council)))
              ,@(and (not (history revolution?))
                     '(("Ask her about Nozomi" boots)))
              ("No" ,nil)))
      (maybe-eri-is-interested-in '...)
      (progn
        (narrate "Yuu can feel Eri slowly disappearing into the classroom.")
        (tsun:tween 2000 255 0
                    (lambda (alpha)
                      (sdl:set-texture-alpha-mod! (tsun:texture eri)
                                                  (round alpha))))
        (tsun:scene-delete! (tsun:current-scene) 'foreground eri)
        (sdl:set-texture-alpha-mod! (tsun:texture eri) 255)))
    (progn
      (play-music 'yuu)
      (narrate "Now Yuu are alone again with nothing to do."))
    (if (history revolution?)
        (progn
          (yuu (: mood) 'mad)
          (narrate "Curse Nozomi!"))
      (progn
        (yuu (: mood) 'normal)
        (narrate "Though it probably doesn't hurt to study in advance.")))
    (progn
      (tsun:tween 1000 0 255
                  (lambda (alpha) (put 'fade 'alpha (round alpha))))
      (tsun:tween 1000 255 0
                  (lambda (alpha) (put 'fade 'alpha (round alpha))))
      (narrate "Approximately an hour passes."))
    (progn
      (play-music 'misaki)
      (tsun:add! 'foreground (misaki (: position) (s32vector 0 360)
                                     (: mood) 'normal))
      (yuu (: mood) 'angry)
      (narrate "In comes your old nemesis.
Cup size K as in \"Kitayama Misaki\".")
      (tsun:tween 2000 0 400
                  (lambda (x)
                    (tsun:move! misaki (s32vector (round x) 360)))))
    (misaki "Yo, how Yuu doin'?")
    (yuu "A Titty Kaiju appeared in front of me.")
    (yuu " I must slay it!" t)
    (misaki "Nice day to Yuu too.")
    (misaki (: mood) 'sad
            "Yuu know, it kinda hurts when Yuu talk about my body like that.")
    (yuu "Well, it's your fault for falling into a cauldron filled with some \
breast-enlarging potion as a child.")
    (misaki (: mood) 'haughty "'Twas mere milk.")
    (misaki (: mood) 'normal
            "\nAlso, contrary to popular belief, milk doesn't give you big \
milkies." t)
    (misaki " That's a lie propagated by big dairy." t)
    (yuu "How do you explain those then?")
    (misaki (: mood) 'sad "That's what I'd like to know.")
    (misaki "It's not like I wanted them.")
    (misaki "All they do is hurt me and my friends.")
    (if (menu pity-misaki?
              `(("Serves you right!" ,nil)
                ("Sounds rough…" ,t)))
        (bond-with-misaki 1)
      (progn
        (misaki (: mood) 'crying "Why do you hate me so much?")
        (tsun:pause)
        (tsun:tween 500 400 -1000
                    (lambda (x)
                      (tsun:move! misaki (s32vector (round x) 360))))
        (tsun:scene-delete! (tsun:current-scene) 'foreground misaki)
        (tsun:call/paused
         (lambda ()
           (narrate "Yuu have defeated the Titty Kaiju.")
           (play-music 'fanfare (: loops) 1)
           (tsun:sleep 12000)
           (narrate "\nYuu don't get any EXP for doing so." t))
         (: key-press) (lambda (&rest _) nil)
         (: key-release) (lambda (&rest _) nil)
         (: mouse-press) (lambda (&rest _) nil)
         (: mouse-release) (lambda (&rest _) nil)
         (: hard-pause?) t)
        (play-music 'yuu))))
  (cond
   ((> (affection eri) 0)
    (tsun:add! 'foreground (eri (: position) (s32vector -320 420)
                                (: mood) 'normal))
    (tsun:tween 2000 -320 320
                (lambda (x)
                  (tsun:move! eri (s32vector (round x) 380))))
    (with-pauses
      (eri "What was that about?")
      (yuu "I have no idea.")
      (eri "Huh…")
      (yuu (: mood) 'happy "…")
      (eri "…"))
    (if (history revolution?)
        (with-pauses
          (eri "How can you be so happy when classes are about to start?")
          (yuu (: mood) 'surprised "Oh my god, classes were about to start!"))
      (eri "Welp, better prepare for classes.")
      (tsun:pause)))
   ((history revolution?)
    (yuu (: mood) 'surprised)
    (narrate "But then Yuu remembered that classes were about to start.")
    (tsun:pause))
   (t (yuu (: mood) 'happy "I should probably prepare for classes now.")
      (tsun:pause)))
  (play-music nil)
  (tsun:tween 2000 0 window-width
              (lambda (end) (put 'fade 'horizontal-end (round end))))
  (tsun:clear! 'foreground)
  (tsun:add! 'foreground (sensei (: position) (s32vector 680 420)
                                 (: mood) 'normal))
  (tsun:tween 2000 0 window-width
              (lambda (start) (put 'fade 'horizontal-start (round start))))
  (put 'fade 'horizontal-start 0)
  (put 'fade 'horizontal-end 0)
  (yuu (: mood) 'normal)
  (play-music 'sensei)
  (with-pauses
    (sensei "So kids, today we will learn why capitalism has always failed \
whenever and wherever it was tried, and why corporate feudalism is the only \
acceptable mode of production.")
    (sensei "But first, please rise to thank our school's sponsor, \
RAID: Shadow Legends.")
    (students "Our lords, who art in the aether,")
    (students "\nhallowed be your names," t)
    (students "\nyour kingdoms come, your will be done," t)
    (students "\nin this land as it is in off-shore tax havens." t)
    (students "Give us this day our daily loot boxes,")
    (students "\nand forgive us for our debts," t)
    (students "\nas we forgive those who overspend." t)
    (students "And lead us not into temptation,")
    (students "\nbut deliver us from Genshin Impact." t)
    (students "For yours are the epic champions and the unique gear and the \
strategic buffs and abilities")
    (students " in eternity." t)
    (students " Amen." t)
    (sensei (: mood) 'happy "Very good!"))
  (when (history revolution?)
    (with-pauses
      (yuu (: mood) 'surprised
           "Uhm, Miss Sensei, what even is a Genshin Impact?")
      (sensei (: mood) 'exhausted "Oh, right, we haven't talked about \
<b>that</b> yet, have we?" (: markup?) t)
      (sensei "Sheesh, where do I begin?")
      (progn
        (yuu (: mood) 'normal)
        (sensei (: mood) 'normal "You see, before the Demon Lord forcefully \
– ahem – <i>open sourced</i> it, Genshin Impact was a hugely influential game."
                (: markup?) t))
      (sensei "Nowadays… well, it'd be a lie to say the demons didn't have \
their fun with it.")
      (sensei "But civilized countries have since put up restrictions against \
it and its many variants.")
      (sensei "You see, once it was – ahem – <i>open sourced</i>, the demons \
could freely alter it in any way they pleased." (: markup?) t)
      (sensei " And that they did." t)
      (sensei "They removed all lootboxes as well as all other forms of \
monetization, instead tying character recruitment mechanics to story progress \
as in the ancient times.")
      (sensei "They produced many more modes and difficulities until the game \
no longer resembled its former self.")
      (sensei "Not to mention the mountains of porn on top of what had \
previously already been produced under less legally clear circumstances.")
      (sensei "It truly was a horror. And still is if you live under \
demon rule.")
      (yuu "Huh…")
      (sensei "Anyway, back to our regular pro– i mean lesson.")))
  (with-pauses
    (sensei "So kids, does anyone of you know who was the first one \
to attempt capitalism?")
    (tsun:say "Student A" "I know, I know!")
    (sensei "Anyone besides Student A?"))
  (if (history revolution?)
      (with-pauses
        (sensei "Okay, you know what? We wasted enough time earlier. \
Student A, you can give us the solution.")
        (tsun:say "Student A" "It was Eagleland.")
        (sensei "Correct, most sources would claim that it was Eagleland."))
    (sensei "Maybe Yuu can answer that for us.")
    (menu pop-quiz-1
          `(("Eagleland" eagleland)
            ("Midgar" midgar)
            ("Ancient Yrup" ancient-yrup)
            ("Emperor Shōwa" showa)))
    (pop-quiz-1 '...))
  (with-pauses
    (sensei "Well then, let's talk about the capitalism of Eagleland.")
    (sensei "Since it does span several centuries and has various phases, \
there's no way to get all the details in a single lesson for 〇〇 graders, \
so we will only do a short summary.")
    (sensei "One note of context: Of course, living in a modern society, \
one would surely claim these people to be primitive, having barely invented \
the inefficient combustion engines that would ultimately lead them to their \
demise.")
    (sensei "Yet at the same time, they could from their point of view be \
described as the pinnacle of then known “civilizations”.")
    (sensei "The founders of Eagleland saw themselves as \
“The Enlightened Ones”, the only “race” capable of leading all others.")
    (sensei "Their main doctrine of racism is what mainly informed this early \
form of capitalism.")
    (sensei "It was not founded on the principles of the the “free market” as \
many proponents would claim, but rather the exploitation of “inferior races”, \
in particular natives.")
    (progn
      (sensei "Yes, natives, for said “Enlightened Ones” all originated from \
the Breit Empire, whose mainland is located in now Yrupean territory.")
      (when (eq (history pop-quiz-1) 'ancient-yrup)
        (tsun:pause)
        (sensei "This is another reason why scholars thought capitalism to \
have come from Yrup even before they found solid evidence for it.")))
    (sensei "For the next decades if not centuries, this style of capitalism \
was predominant throughout Eagleland.")
    (sensei "It caused tremendous amounts of suffering in humans, animals \
and nature as a whole, and parts of it even persisted into later phases.")
    (sensei "Which brings us to the second phase of capitalism…")
    (progn
      (yuu (: mood) 'sad)
      (narrate "The lesson continues like that, but that'd be more exposition \
than Yuu can handle.")))
  (cond
   ((eq (history maybe-eri-is-interested-in) 'skipping-classes)
    (with-pauses
      (narrate " So Yuu don't." t)
      (yuu "Ms. Sensei, I'm not feeling well. Can I visit the school nurse?")
      (sensei "Again? That must be the fourth time this week you've asked.")
      (sensei "And the answer always remains the same: We have no school \
nurse, because if we did, little shits like Yuu would just abuse their \
existence to skip class.")
      (narrate "Once again your plans were foiled.")))))

(defun maybe-eri-is-interested-in (&rest _)
  (let ((what (history maybe-eri-is-interested-in)))
    (cond
     ((eq what 'smalltalk)
      (yuu (: mood) 'happy "Today's weather is nice, isn't it?")
      (tsun:pause)
      (affection eri -100)
      (eri "…")
      (tsun:pause))
     ((eq what 'skipping-classes)
      (yuu (: mood) 'mad "But really, why do we need to be in school at 7am?")
      (tsun:pause)
      (eri (: mood) 'shy "I… don't quite understand that one either.")
      (affection eri +5)
      (tsun:pause))
     ((eq what 'challenging-the-student-council)
      (yuu (: mood) 'mad "But really, who does Nozomi think she is?")
      (tsun:pause)
      (affection eri -1)
      (eri "Uhh… the student council president?")
      (tsun:pause))
     ((eq what 'boots)
      (with-pauses
        (yuu (: mood) 'normal "So, what's up with Nozomi?")
        (eri (: mood) 'shy "Well, she's the student council president…")
        (eri " But you probably know that already…" t)
        (eri "She really hates it when you're late for class.")
        (eri " Well, we both wouldn't be here at this hour if she didn't." t)
        (eri "Uhm…")
        (yuu (: mood) 'surprised "Wow, you know quite a lot.")
        (progn
          (eri (: mood) 'blushing "…")
          (affection eri +1))))
     (t
      (narrate "Maybe next time.")
      (tsun:pause)))))

(defun bond-with-misaki (&rest _)
  (affection misaki +5)
  (misaki "Yuu… believe me?")
  (tsun:pause)
  (misaki (: mood) 'deredere "Yuu actually believe me!")
  (tsun:pause)
  (tsun:tween 200 400 800
              (lambda (x)
                (tsun:move! misaki (s32vector (round x) 360)))
              (: ease) (@ (guile) identity))
  (yuu (: mood) 'overtaken)
  (misaki "Oh, I love Yuu, Yuu are the cutest and kindest girl I know.")
  (tsun:pause)
  (misaki (: mood) 'embarrassed)
  (tsun:tween 800 800 600
              (lambda (x)
                (tsun:move! misaki (s32vector (round x) 360))))
  (misaki "D-did I just say that out loud?")
  (tsun:pause)
  (misaki "Oh my gosh! I'm so sorry, I'm so sorry…")
  (tsun:tween 500 400 -1000
              (lambda (x)
                (tsun:move! misaki (s32vector (round x) 360))))
  (tsun:scene-delete! (tsun:current-scene) 'foreground misaki)
  (tsun:pause)
  (tsun:call/paused
   (lambda ()
     (narrate "Yuu were defeated by the Titty Kaiju.")
     (play-music nil)
     (play-sound 'game-over)
     (tsun:sleep 6000)
     (narrate "\nYuu gained a valuable experience." t))
   (: key-press) (lambda (&rest _) nil)
   (: key-release) (lambda (&rest _) nil)
   (: mouse-press) (lambda (&rest _) nil)
   (: mouse-release) (lambda (&rest _) nil)
   (: hard-pause?) t)
  (play-music 'yuu))

(defun pop-quiz-1 (&rest _)
  (let ((what (history pop-quiz-1)))
    (cond
     ((eq what 'eagleland)
      (sensei "Correct, most sources would claim that it was Eagleland.")
      (affection sensei +1)
      (tsun:pause))
     ((eq what 'midgar)
      (sensei "No, Midgar is sadly wrong.")
      (tsun:pause)
      (sensei "There are huge debates about whether it even experienced \
“real capitalism” throughout most of history.")
      (sensei "But even if it did, capitalism had already had decade if not \
centuries of development in Eagleland before its adoption in Midgar.")
      (tsun:pause)
      (sensei "And of course, the demons that now live in Midgar have \
replaced it with <i>their system</i> anyway." (: markup?) t)
      (tsun:pause))
     ((eq what 'showa)
      (sensei "Hahaha, no.")
      (affection sensei -5)
      (tsun:pause)
      (sensei "\nThe correct answer would be Eagleland." t)
      (tsun:pause))
     ((eq what 'ancient-yrup)
      (sensei "Y-you didn't just guess, did you?")
      (affection sensei +5)
      (tsun:pause)
      (sensei "Ahem, indeed, while many sources claim that capitalism \
originated in Eagleland, historians found earlier records describing it in \
Ancient Yrup.")
      (tsun:pause)
      (sensei "However, our school books don't have that yet, so for now \
we'll assume that it originated in Eagleland as the book tells us.")
      (tsun:pause)))))

(module-set-variable! (current-module) 'game-name)
(module-set-variable! (current-module) 'window-width)
(module-set-variable! (current-module) 'window-height)
(module-set-function! (current-module) 'init!)
